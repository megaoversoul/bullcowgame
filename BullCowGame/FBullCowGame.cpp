#pragma once
#include "FBullCowGame.h"

// Syntax used to meet Unreal's coding standards.
#include <map>
#define TMap std::map

FBullCowGame::FBullCowGame() { Reset(); } // Default constructor.


int32 FBullCowGame::GetHiddenWordLength() const { return MyHiddenWord.length(); }
int32 FBullCowGame::GetCurrentTry() const { return MyCurrentTry; }
bool FBullCowGame::IsGameWon() const { return bGameIsWon; }


int32 FBullCowGame::GetMaxTries() const // Getting the max tries, determined by the number of letters in the hidden word.
{ 
	TMap<int32, int32> WordLengthToMaxTries{ {2,4}, {3,5}, {4,7}, {5,10}, {6,16}, {7,20}, {8, 25} };
	return WordLengthToMaxTries[MyHiddenWord.length()]; 
}

void FBullCowGame::Reset() // Resetting the private variables.
{
	const FString HIDDEN_WORD = "planet"; // This MUST be an isogram.

	MyHiddenWord = HIDDEN_WORD;
	bGameIsWon = false;
	MyCurrentTry = 1;

	return;
}

void FBullCowGame::ResetEasy() // Resetting the Easy difficulty word.
{
	const FString HIDDEN_WORD = "love"; // This MUST be an isogram.
	MyHiddenWord = HIDDEN_WORD;

	return;
}

void FBullCowGame::ResetMedium() // Resetting the Medium difficulty word.
{
	const FString HIDDEN_WORD = "print"; // This MUST be an isogram.
	MyHiddenWord = HIDDEN_WORD;

	return;
}

void FBullCowGame::ResetHard() // Resetting the Hard difficulty word.
{
	const FString HIDDEN_WORD = "basket"; // This MUST be an isogram.
	MyHiddenWord = HIDDEN_WORD;

	return;
}

// Player chooses their difficulty level.
EDifficultyStatus FBullCowGame::CheckDifficulty(FString Difficulty) const
{
	if (Difficulty[0] == 'e')
	{
		return EDifficultyStatus::Easy;
	}
	else if (Difficulty[0] == 'E')
	{
		return EDifficultyStatus::Easy;
	}
	else if (Difficulty[0] == 'm')
	{
		return EDifficultyStatus::Medium;
	}
	else if (Difficulty[0] == 'M')
	{
		return EDifficultyStatus::Medium;
	}
	else if (Difficulty[0] == 'h')
	{
		return EDifficultyStatus::Hard;
	}
	else if (Difficulty[0] == 'H')
	{
		return EDifficultyStatus::Hard;
	}
	return EDifficultyStatus();
}

EGuessStatus FBullCowGame::CheckGuessValidity(FString Guess) const // Checking the valitity of the player's guess.
{
	if (!IsIsolgram(Guess)) // If the guess isn't an isogram.
	{
		return EGuessStatus::IsIsogram;
	}
	else if (!IsLowercase(Guess)) // If the guess isn't all lowercase.
	{
		return EGuessStatus::Not_Lowercase;
	}
	else if (Guess.length() != GetHiddenWordLength()) // If the guess length is wrong.
	{
		return EGuessStatus::Not_Length;
	}
	else // Otherwise.
	{
		return EGuessStatus::Ok;
	}
}

// Receives a VALID guess, increments turn, and returns count.
FBullCowCount FBullCowGame::SubmitValidGuess(FString Guess)
{
	MyCurrentTry++; // Increment the turn number.
	FBullCowCount BullCowCount; // Setup a return variable.
	int32 WordLength = MyHiddenWord.length(); // Assuming the same length as guess.

	// Loop through all the letters in the hidden word.
	for (int32 MHWChar = 0; MHWChar < WordLength; MHWChar++)
	{
		for (int32 GChar = 0; GChar < WordLength; GChar++) // Compare letters against the guess.
		{
			if (Guess[GChar] == MyHiddenWord[MHWChar]) // If they match then:
			{
				if (MHWChar == GChar) 
				{
					BullCowCount.Bulls++; // Increment Bulls.
				}
				else 
				{
					BullCowCount.Cows++; // Increment Cows.
				}
			}
		}
	}
	if (BullCowCount.Bulls == WordLength) 
	{
		bGameIsWon = true;
	}
	else
	{
		bGameIsWon = false;
	}
	return BullCowCount;
}

bool FBullCowGame::IsIsolgram(FString Word) const
{
	if (Word.length() <= 1) { return true; } // Treat 1 and 0 letter words as isograms.

	TMap<char, bool> LetterSeen; // Set up our map.
	for (auto Letter : Word) // Loop through all the letters of the guess.
	{
		Letter = tolower(Letter); // Handle mixed case words.
		if (LetterSeen[Letter]) // If the letter is in the map
		{
			return false; // we do NOT have an isogram,
		} else {
			LetterSeen[Letter] = true; // add the letter to the map as seen.
		}
	}
	return true;
}

bool FBullCowGame::IsLowercase(FString Word) const
{
	for (auto Letter : Word)
	{
		if (!islower(Letter))// If not a lowercase letter
		{
			return false;
		}
	}
	return true;
}
