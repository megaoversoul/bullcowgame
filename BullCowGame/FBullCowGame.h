/* The game code with no view code or direct user interaction.
The game is a simple guess the word game based on Mastermind.
*/

#pragma once
#include <string>

// Syntax used to meet Unreal's coding standards.
using FString = std::string;
using int32 = int;

struct FBullCowCount
{
	int32 Bulls = 0;
	int32 Cows = 0;
};

enum class EDifficultyStatus
{
	Invalid_Difficulty_Status,
	Easy,
	Medium,
	Hard
};

enum class EGuessStatus
{
	Invalid_Status,
	Ok,
	IsIsogram,
	Not_Length,
	Not_Lowercase
};

class FBullCowGame
{
public:
	FBullCowGame(); // Constructor.

	int32 GetMaxTries() const;
	int32 GetHiddenWordLength() const;
	int32 GetCurrentTry() const;
	bool IsGameWon() const;
	EDifficultyStatus CheckDifficulty(FString) const;
	EGuessStatus CheckGuessValidity(FString) const;

	void Reset();
	void ResetEasy();
	void ResetMedium();
	void ResetHard();
	FBullCowCount SubmitValidGuess(FString);

private:
	int32 MyCurrentTry;
	FString MyHiddenWord;
	bool bGameIsWon;

	bool IsIsolgram(FString) const;
	bool IsLowercase(FString) const;
};
