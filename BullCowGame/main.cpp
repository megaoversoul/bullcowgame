/* This is the console executable makes use of the BullCow class.
This acts as a view in the MVC pattern and is responsible for all user interaction.
For game logic, see the FBullCowGame class.
*/

#pragma once
#include <iostream>
#include <string>
#include "FBullCowGame.h"

// Syntax used to meet Unreal's coding standards.
using FText = std::string;
using int32 = int;

// Function prototypes as outside of a class.
void PrintIntro();
void PlayGame();
FText GetValidGuess();
FText GetDifficulty();
bool AskToPlayAgain();
void PrintGameSummary();

FBullCowGame BCGame; // Instantiate a new game.


int main() // The entry point of the application.
{
	bool bPlayAgain = false;
	do
	{
		PrintIntro();
		PlayGame();
		bPlayAgain = AskToPlayAgain();
	} 
	while (bPlayAgain);

	return 0; // Exit the application.
}


void PrintIntro() // Introduce the game.
{
	std::cout << "\n                       Welcome to\n";
	std::cout << "     __                                                 \n";
	std::cout << "    |  \\                         /\\                    \n";
	std::cout << "    |  /                        /  \\                    \n";
	std::cout << "    | /                         |                        \n";
	std::cout << "    | \\  |  | |   |    /        |     /\\  |  |   /             \n";
	std::cout << "    |  \\ |  | |   |   /-/  |\\|  \\  / |  | |  |  /-/            \n";
	std::cout << "    |__/ |__| |__ |__  /         \\/   \\/  \\/\\/   /                  \n";
	std::cout << std::endl;
	std::cout << "     Can you guess the word that I'm thinking of?\n";
	std::cout << std::endl;
}


void PlayGame() // Plays a single game to completion.
{
	BCGame.Reset();
	// Player chooses the word they'll be guessing by choosing their difficulty level.
	FText Difficulty = GetDifficulty();

	int32 MaxTries = BCGame.GetMaxTries();

	// Loop asking for guesses while the game is not won and tries still remain.
	while (!BCGame.IsGameWon() && BCGame.GetCurrentTry() <= MaxTries)
	{
		// Returning the players guess.
		FText Guess = GetValidGuess(); // A check to see if a guess is valid.

		// Submit a valid guess to the game.
		FBullCowCount BullCowCount = BCGame.SubmitValidGuess(Guess);

		// Print number of bulls and cows.
		std::cout << "Bulls = " << BullCowCount.Bulls;
		std::cout << ". Cows = " << BullCowCount.Cows << ".\n\n";
	}
	PrintGameSummary();
	return; 
}


FText GetDifficulty()
{
	FText Difficulty = "";
	EDifficultyStatus DifficultyStatus = EDifficultyStatus::Invalid_Difficulty_Status;

	do 
	{
		std::cout << "Choose your skill level. Easy, Medium, or Hard. (e/m/h): ";
		std::getline(std::cin, Difficulty);
		std::cout << std::endl;

		DifficultyStatus = BCGame.CheckDifficulty(Difficulty);

		switch (DifficultyStatus)
		{
		case EDifficultyStatus::Easy:
			BCGame.ResetEasy();
			std::cout << "You chose Easy difficulty. Please guess a " << BCGame.GetHiddenWordLength() << " letter word.\n\n";
			break;
		case EDifficultyStatus::Medium:
			BCGame.ResetMedium();
			std::cout << "You chose Medium difficulty. Please guess a " << BCGame.GetHiddenWordLength() << " letter word.\n\n";
			break;
		case EDifficultyStatus::Hard:
			BCGame.ResetHard();
			std::cout << "You chose Hard difficulty. Please guess a " << BCGame.GetHiddenWordLength() << " letter word.\n\n";
			break;
		default:
			break;
		}

	} while (DifficultyStatus == EDifficultyStatus::Invalid_Difficulty_Status);

	return Difficulty;
}

FText GetValidGuess() // Loop until the user gives a valid guess.
{
	FText Guess = "";
	EGuessStatus Status = EGuessStatus::Invalid_Status;
	do {
		// Get a guess from the player.
		int32 MyCurrentTry = BCGame.GetCurrentTry();
		int32 MyMaxTries = BCGame.GetMaxTries();
		std::cout << "Try " << MyCurrentTry << " of " << MyMaxTries << ". Type your guess here: ";
		std::getline(std::cin, Guess);

		Status = BCGame.CheckGuessValidity(Guess);

		switch (Status)
		{
		case EGuessStatus::Not_Length:
			std::cout << "Please enter a " << BCGame.GetHiddenWordLength() << " letter word.\n\n";
			break;
		case EGuessStatus::IsIsogram:
			std::cout << "Please enter a word with no two letters being the same.\n\n";
			break;
		case EGuessStatus::Not_Lowercase:
			std::cout << "Please enter a word in all lowercase.\n\n";
			break;
		default:
			break;
		}
	} while (Status != EGuessStatus::Ok);
	return Guess;
}


bool AskToPlayAgain() // Ask the player to play again.
{
	FText Response = "";
	std::cout << "Do you want to play again? (y/n) ";
	std::getline(std::cin, Response);
	std::cout << std::endl;
	return (Response[0] == 'y') || (Response[0] == 'Y');
}

void PrintGameSummary() 
{
	if (BCGame.IsGameWon())
	{
		std::cout << "YOU WIN! CONGRATULATIONS!!!\n";
	}
	else
	{
		std::cout << "Sorry, you didn't guess the word. Better luck next time!\n";
	}
}
